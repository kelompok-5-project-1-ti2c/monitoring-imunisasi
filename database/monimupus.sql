-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 10, 2019 at 09:34 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `monimupus`
--

-- --------------------------------------------------------

--
-- Table structure for table `anak`
--

CREATE TABLE `anak` (
  `anak_id` int(11) NOT NULL,
  `no_kk` varchar(30) NOT NULL,
  `nama_anak` varchar(40) NOT NULL,
  `tempat_lahir` varchar(30) NOT NULL,
  `tgl_lahir` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `anak`
--

INSERT INTO `anak` (`anak_id`, `no_kk`, `nama_anak`, `tempat_lahir`, `tgl_lahir`) VALUES
(1, '1234567890123123', 'Ali Basalamah', 'Cirebon', '2019-11-14'),
(2, '1234567890124124', 'Gofar Ismail', 'Cirebon', '2019-11-18'),
(3, '1234567890124124', 'Dony Syahputra Priyadi', 'Cirebon', '2019-11-18'),
(4, '1234567890111111', 'Wiwit Fitria', 'Indramayu', '2019-11-17'),
(5, '1234567890222222', 'Dony Jauhari Putri', 'Cirebon', '2019-11-18'),
(6, '1234567890222222', 'Gofar Ismail', 'Cirebon', '2019-12-10');

-- --------------------------------------------------------

--
-- Table structure for table `desa`
--

CREATE TABLE `desa` (
  `desa_id` int(11) NOT NULL,
  `kode_puskesmas` varchar(8) NOT NULL,
  `nama_desa` varchar(30) NOT NULL,
  `status` enum('on','off') NOT NULL DEFAULT 'on'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `desa`
--

INSERT INTO `desa` (`desa_id`, `kode_puskesmas`, `nama_desa`, `status`) VALUES
(1, 'LHB12345', 'Bojongslawi', 'on'),
(2, 'LHB12345', 'Kiajaran Kulon', 'on'),
(3, 'LHB12345', 'Kiajaran Wetan', 'on'),
(4, 'LHB12345', 'Langut', 'on'),
(5, 'LHB12345', 'Lanjan', 'on'),
(6, 'LHB12345', 'Larangan', 'on'),
(7, 'LHB12345', 'Legok', 'on'),
(8, 'LHB12345', 'Lohbener', 'on'),
(9, 'LHB12345', 'Pamayahan', 'on'),
(10, 'LHB12345', 'Rambatan Kulon', 'on'),
(11, 'LHB12345', 'Sindangkerta', 'on'),
(12, 'LHB12345', 'Waru', 'on'),
(16, 'CNT12345', 'Panyingkiran Lor', 'on');

-- --------------------------------------------------------

--
-- Table structure for table `imunisasi`
--

CREATE TABLE `imunisasi` (
  `kode_imunisasi` varchar(8) NOT NULL,
  `nama_imunisasi` varchar(30) NOT NULL,
  `bulan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `imunisasi`
--

INSERT INTO `imunisasi` (`kode_imunisasi`, `nama_imunisasi`, `bulan`) VALUES
('BCG-00', 'BCG', 0),
('CMP-00', 'Campak', 9),
('DPT-HB1', 'DPT/HB1', 2),
('DPT-HB2', 'DPT/HB2', 3),
('DPT-HB3', 'DPT/HB3', 4),
('HB-00', 'HB 0', 0),
('POL-01', 'Polio 1', 0),
('POL-02', 'Polio 2', 2),
('POL-03', 'Polio 3', 3),
('POL-04', 'Polio 4', 4);

-- --------------------------------------------------------

--
-- Table structure for table `keluarga`
--

CREATE TABLE `keluarga` (
  `no_kk` varchar(30) NOT NULL,
  `nama_ayah` varchar(40) NOT NULL,
  `nama_ibu` varchar(40) NOT NULL,
  `no_hp` varchar(13) NOT NULL,
  `alamat` text NOT NULL,
  `desa` varchar(30) NOT NULL,
  `kecamatan` varchar(40) NOT NULL,
  `status` enum('on','off') NOT NULL DEFAULT 'on'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `keluarga`
--

INSERT INTO `keluarga` (`no_kk`, `nama_ayah`, `nama_ibu`, `no_hp`, `alamat`, `desa`, `kecamatan`, `status`) VALUES
('1234567890111111', 'Huesain Haekal', 'Fulani', '08123456789', 'Pamayahan RT/RW 01/01', 'Pamayahan', 'Lohbener', 'on'),
('1234567890123123', 'Sobri Basalamah', 'Nilma Bareyyek', '08123456789', 'Bojongslawi, RT/RW 01/02', 'Bojongslawi', 'Lohbener', 'on'),
('1234567890124124', 'Kafabih', 'Fulana', '08123456789', 'Pamayahan', 'Pamayahan', 'Lohbener', 'on'),
('1234567890222222', 'Sidiq Jauhari', 'Alya', '08123456780', 'Bojongslawi, RT/RW 01/02', 'Bojongslawi', 'Lohbener', 'on'),
('263427327472427', 'Ibrahim', 'Hanifah', '08123456789', 'Pamayahan', 'Pamayahan', 'Lohbener', 'on');

--
-- Triggers `keluarga`
--
DELIMITER $$
CREATE TRIGGER `insert` AFTER INSERT ON `keluarga` FOR EACH ROW INSERT INTO keluarga_log (no_kk, nama_ayah, nama_ibu, no_hp, alamat, desa, kecamatan, status, kejadian, waktu) VALUES (NEW.no_kk, NEW.nama_ayah, NEW.nama_ibu, NEW.no_hp, NEW.alamat, NEW.desa, NEW.kecamatan, NEW.status, 'Menambahkan data', now())
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update` AFTER UPDATE ON `keluarga` FOR EACH ROW INSERT INTO keluarga_log (no_kk, nama_ayah, nama_ibu, no_hp, alamat, desa, kecamatan, status, kejadian, waktu) VALUES (OLD.no_kk, OLD.nama_ayah, OLD.nama_ibu, OLD.no_hp, OLD.alamat, OLD.desa, OLD.kecamatan, OLD.status, 'Mengubah data', now())
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `keluarga_log`
--

CREATE TABLE `keluarga_log` (
  `keluarga_log_id` int(11) NOT NULL,
  `no_kk` varchar(30) NOT NULL,
  `nama_ayah` varchar(40) NOT NULL,
  `nama_ibu` varchar(40) NOT NULL,
  `no_hp` varchar(13) NOT NULL,
  `alamat` text NOT NULL,
  `desa` varchar(40) NOT NULL,
  `kecamatan` varchar(40) NOT NULL,
  `status` enum('on','off') NOT NULL,
  `kejadian` varchar(1000) NOT NULL,
  `waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `keluarga_log`
--

INSERT INTO `keluarga_log` (`keluarga_log_id`, `no_kk`, `nama_ayah`, `nama_ibu`, `no_hp`, `alamat`, `desa`, `kecamatan`, `status`, `kejadian`, `waktu`) VALUES
(1, '1234567890222222', 'Sidiq Jauhari', 'Alya', '08123456789', 'Bojongslawi, RT/RW 01/02', 'Bojongslawi', 'Lohbener', 'on', 'Menambahkan data', '2019-11-18 11:12:09'),
(2, '1234567890111111', 'Haekal', 'Fulani', '08123456789', 'Pamayahan RT/RW 01/01', 'Pamayahan', 'Lohbener', 'on', 'Mengubah data', '2019-11-18 12:16:57'),
(3, '1234567890222222', 'Sidiq Jauhari', 'Alya', '08123456789', 'Bojongslawi, RT/RW 01/02', 'Bojongslawi', 'Lohbener', 'on', 'Mengubah data', '2019-11-18 17:05:23'),
(4, '263427327472427', 'Ibrahim', 'Hanifah', '08xxxxxxxxxx', 'e gdcjkwg dewgfdu weqgf ewhjqgf u', 'Waru', 'Lohbener', 'on', 'Menambahkan data', '2019-11-18 17:29:14'),
(5, '263427327472427', 'Ibrahim', 'Hanifah', '08xxxxxxxxxx', 'e gdcjkwg dewgfdu weqgf ewhjqgf u', 'Waru', 'Lohbener', 'on', 'Mengubah data', '2019-11-18 17:30:27');

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `petugas_id` int(11) NOT NULL,
  `kode_puskesmas` varchar(8) NOT NULL,
  `desa_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `nama_petugas` varchar(30) NOT NULL,
  `status` enum('on','off') NOT NULL DEFAULT 'on'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`petugas_id`, `kode_puskesmas`, `desa_id`, `username`, `password`, `nama_petugas`, `status`) VALUES
(1, 'LHB12345', 1, '0', '0', 'Devika Meina', 'on'),
(4, 'LHB12345', 8, 'adityarf', '21232f297a57a5a743894a0e4a801fc3', 'Aditya Rifqy Fauzan', 'on'),
(5, 'LHB12345', 10, '0', '0', 'Abu Mushonnip', 'off');

-- --------------------------------------------------------

--
-- Table structure for table `puskesmas`
--

CREATE TABLE `puskesmas` (
  `kode_puskesmas` varchar(8) NOT NULL,
  `nama_puskesmas` varchar(30) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `alamat` text NOT NULL,
  `kecamatan` varchar(30) NOT NULL,
  `status` enum('on','off') NOT NULL DEFAULT 'on'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `puskesmas`
--

INSERT INTO `puskesmas` (`kode_puskesmas`, `nama_puskesmas`, `password`, `alamat`, `kecamatan`, `status`) VALUES
('CNT12345', 'Cantigi', '21232f297a57a5a743894a0e4a801fc3', 'Cantigi', 'Cantigi', 'on'),
('LHB12345', 'Lohbener', '21232f297a57a5a743894a0e4a801fc3', 'Lohbener', 'Lohbener', 'on');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anak`
--
ALTER TABLE `anak`
  ADD PRIMARY KEY (`anak_id`),
  ADD KEY `anak_ibfk_1` (`no_kk`);

--
-- Indexes for table `desa`
--
ALTER TABLE `desa`
  ADD PRIMARY KEY (`desa_id`),
  ADD KEY `kode_puskesmas` (`kode_puskesmas`);

--
-- Indexes for table `imunisasi`
--
ALTER TABLE `imunisasi`
  ADD PRIMARY KEY (`kode_imunisasi`);

--
-- Indexes for table `keluarga`
--
ALTER TABLE `keluarga`
  ADD PRIMARY KEY (`no_kk`);

--
-- Indexes for table `keluarga_log`
--
ALTER TABLE `keluarga_log`
  ADD PRIMARY KEY (`keluarga_log_id`),
  ADD KEY `no_kk` (`no_kk`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`petugas_id`),
  ADD KEY `kode_puskesmas` (`kode_puskesmas`),
  ADD KEY `desa_id` (`desa_id`);

--
-- Indexes for table `puskesmas`
--
ALTER TABLE `puskesmas`
  ADD PRIMARY KEY (`kode_puskesmas`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anak`
--
ALTER TABLE `anak`
  MODIFY `anak_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `desa`
--
ALTER TABLE `desa`
  MODIFY `desa_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `keluarga_log`
--
ALTER TABLE `keluarga_log`
  MODIFY `keluarga_log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `petugas_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `anak`
--
ALTER TABLE `anak`
  ADD CONSTRAINT `anak_ibfk_1` FOREIGN KEY (`no_kk`) REFERENCES `keluarga` (`no_kk`);

--
-- Constraints for table `desa`
--
ALTER TABLE `desa`
  ADD CONSTRAINT `desa_pus` FOREIGN KEY (`kode_puskesmas`) REFERENCES `puskesmas` (`kode_puskesmas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `keluarga_log`
--
ALTER TABLE `keluarga_log`
  ADD CONSTRAINT `log_keluarga` FOREIGN KEY (`no_kk`) REFERENCES `keluarga` (`no_kk`);

--
-- Constraints for table `petugas`
--
ALTER TABLE `petugas`
  ADD CONSTRAINT `petugas_des` FOREIGN KEY (`desa_id`) REFERENCES `desa` (`desa_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `petugas_pus` FOREIGN KEY (`kode_puskesmas`) REFERENCES `puskesmas` (`kode_puskesmas`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
