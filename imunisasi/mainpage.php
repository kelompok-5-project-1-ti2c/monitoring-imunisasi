<?php

    include_once("../function/helper.php");
    include_once("../function/imunisasi_function.php");

    session_start();
    $nama_petugas = $_SESSION['nama_petugas'];
    $kode_puskesmas = $_SESSION['kode_puskesmas'];
    $desa_id = $_SESSION['desa_id'];

    $alert = isset($_GET['alert']) ? $_GET['alert'] : false;
    if($alert == 'selamat-datang'){
        $swal = "
            <script>
                Swal.fire({
                  showCloseButton:true, 
                  type: 'success',
                  title: 'Selamat Datang',
                  text: 'Selamat datang $nama_petugas'
                });
            </script>
        ";
       }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel='stylesheet' type='text/css' media='screen' href='<?= BASE_URL; ?>assets/css/bootstrap.min.css'>
    <link rel='stylesheet' type='text/css' media='screen' href='<?= BASE_URL; ?>assets/css/login.css'>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src='<?= BASE_URL; ?>assets/js/jquery.min.js'></script>
    <script src='<?= BASE_URL; ?>assets/js/popper.min.js'></script>
    <script src='<?= BASE_URL; ?>assets/js/bootstrap.min.js'></script>
</head>
<body>
    <?= $swal; ?>
    
    

</body>
</html>