<?php
	error_reporting(0);
	isset($_GET['alert']) ? $_GET['alert'] : false;
	$alert = $_GET['alert'];

	session_start();
	$sesi = isset($_SESSION['kode_puskesmas']) ? $_SESSION['kode_puskesmas'] : false;
	echo $sesi;
	if($sesi){
		header("location: module/index.php?page=home");
	}


	if($alert == 'gagal'){
		$swal = "
			<script>
				Swal.fire({
				  showCloseButton:true, 
				  type: 'error',
				  title: 'Oops...',
				  text: 'Username atau Password salah!',
				});
			</script>
		";
	}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Monitoring Imunisasi</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='assets/css/bootstrap.min.css'>
    <link rel='stylesheet' type='text/css' media='screen' href='assets/css/login.css'>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src='assets/js/jquery.min.js'></script>
    <script src='assets/js/popper.min.js'></script>
    <script src='assets/js/bootstrap.min.js'></script>

</head>
<body class="body">

<?php echo $swal; ?>

<div class="container">

		<div class="row">
			
			<div class="col-md-4 pl-md-0">
				
            </div>
			<div class="col-md-4 p-md-0">
				
				<div class="card card-info">
					
					<div class="card-header bg-info text-center"><h4>Login</h4></div>
					<div class="card-body">
						
						<p class="text-center"><img src="images/img-assets/hospital.png" width="35%">

					<form method="post" action="login.php">
						<div class="form-group">
							<label class="label" for="kode_puskesmas">Kode Puskesmas :</label>
	                        <input type="text" placeholder="Cth : LHB123xxx" autocomplete="off" autofocus="autofocus" required="required" class="form-control" name="kode_puskesmas" id="kode_puskesmas">
	                    </div>
	                    <div class="form-group">
	                        <label class="label" for="password">Password :</label>
	                        <input type="password" placeholder="Password" class="form-control" id="password" required="required" name="password">
	                    </div>
	                    <div class="form-group">
	                    	<button type="submit" class="btn btn-info btn-login" name="button">
		                        <h5>Masuk</h5>
		                    </button>
	                    </div>
	                </form>

					</div>
					<div class="card-footer footer">
						<p>Copyright &copy;</c> 2019 <b>monitoring-imunisasi</b></p>
					</div>

				</div>

			</div>
			<div class="col-md-4 pl-md-0">
				
            </div>

		</div>

	</div>
    
</body>
</html>