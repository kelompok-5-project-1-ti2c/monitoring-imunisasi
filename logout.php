<?php
	
	include_once("function/helper.php");
	session_start();

	unset($_SESSION['nama_puskesmas']);
	unset($_SESSION['kode_puskesmas']);
	unset($_SESSION['username']);
	unset($_SESSION['password']);

	header("location:".BASE_URL);

?>