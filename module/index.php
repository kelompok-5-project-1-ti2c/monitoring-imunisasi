<?php
    error_reporting(0);    
    include_once("../function/helper.php");
    include_once("../function/koneksi.php");
    
    session_start();

    $nama_puskesmas = $_SESSION['nama_puskesmas'];
    $kode_puskesmas = $_SESSION['kode_puskesmas'];
    $kecamatan      = $_SESSION['kecamatan'];
    if($nama_puskesmas){

    }else {
        header("location:".BASE_URL);
    }

    $page = isset($_GET['page']) ? $_GET['page'] : false;
    $desa = isset($_GET['desa']) ? $_GET['desa'] : false;
    
    if($desa){
        header("location:".BASE_URL."module/index.php?page=keluarga&sort=$desa");
    }
    $sort = isset($_GET['sort']) ? $_GET['sort'] : false;
    
    $alert = isset($_GET['alert']) ? $_GET['alert'] : false;

    if($alert == 'berhasil'){
        $swal = "
            <script>
                Swal.fire({
                  showCloseButton:true, 
                  type: 'success',
                  title: 'Selamat Datang',
                  text: 'Selamat datang Puskesmas $nama_puskesmas'
                });
            </script>
        ";
       }if($alert == 'save-berhasil'){
        $swal = "
            <script>
                Swal.fire({
                  showCloseButton:true, 
                  type: 'success',
                  title: 'Success',
                  text: 'Data Berhasil Disimpan'
                });
            </script>
        ";
       }if($alert == 'update-berhasil'){
        $swal = "
            <script>
                Swal.fire({
                  showCloseButton:true, 
                  type: 'success',
                  title: 'Success',
                  text: 'Data Berhasil Diperbarui'
                });
            </script>
        ";
       }if($alert == 'hapus'){
        $swal = "
            <script>
            Swal.fire({
                title: 'Are you sure?',
                text: 'Hello',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                if (result.value) {
                    Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                    )
                }
              });
            </script>
        ";
       }

    if($page == 'home'){
        $home      = 'active';
        $imunisasi = '';
        $desa      = '';
        $keluarga  = '';
        $laporan   = '';
        $petugas   = '';
    }else if($page == 'imunisasi'){
        $home      = '';
        $imunisasi = 'active';
        $desa      = '';
        $keluarga  = '';
        $laporan   = '';
        $petugas   = '';
    }else if($page == 'desa'){
        $home      = '';
        $imunisasi = '';
        $desa      = 'active';
        $keluarga  = '';
        $petugas   = '';
        $laporan   = '';
    }else if($page == 'keluarga'){
        $home      = '';
        $imunisasi = '';
        $desa      = '';
        $petugas   = '';
        $keluarga  = 'active';
        $laporan   = '';
    }else if($page == 'petugas'){
        $home      = '';
        $imunisasi = '';
        $desa      = '';
        $petugas  = 'active';
        $laporan   = '';
    }else if($page == 'laporan'){
        $petugas   = '';
        $home      = '';
        $imunisasi = '';
        $desa      = '';
        $keluarga  = '';
        $laporan   = 'active';
    }   
	

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Monitoring Imunisasi</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='<?php echo BASE_URL; ?>/assets/css/bootstrap.min.css'>
    <link rel='stylesheet' type='text/css' media='screen' href='<?php echo BASE_URL; ?>/assets/css/main.css'>
    <link rel='stylesheet' type='text/css' media='screen' href='<?php echo BASE_URL; ?>/assets/css/all.css'>
    <link rel='stylesheet' type='text/css' media='screen' href='<?php echo BASE_URL; ?>/assets/datatables/dataTables.bootstrap4.min.css'>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src='<?php echo BASE_URL; ?>/assets/js/bootstrap.min.js'></script>
    <script src='<?php echo BASE_URL; ?>/assets/js/jquery.min.js'></script>
    <script src='<?php echo BASE_URL; ?>/assets/datatables/jquery.dataTables.min.js'></script>
    <script src='<?php echo BASE_URL; ?>/assets/datatables/dataTables.bootstrap4.min.js'></script>
</head>
<body class="body">

    <?php echo $swal; ?>
    
    <nav class="navbar fixed-top flex-md-nowrap pl-0 shadow">
        <a class="navbar-brand col-sm-3 col-md-2 text-center"><b>MO<span class="pus">IMU</span>PUS</b></a>
    </nav>
    <div class="container-fluid">

        <div class="row menu">

            <div class="col-md-2 navside">
                <p class="text-center puskesmas">
                    <img src="../images/img-assets/hospital-1.png" class="img-user" width="35%"></br>
                    PUSKESMAS <b><?php echo strtoupper($nama_puskesmas); ?></b>
                </p>
                <p class="text-center">
                    <a href="<?php echo BASE_URL."logout.php" ?>" class="btn btn-danger btn-logout"><i class="fas fa-sign-out-alt"></i> Logout</a>
                </p>
                <div class="list-group">
                    <a href="<?php echo BASE_URL."module/index.php?page=home" ?>" class="list-group-item list-group-item-action data <?php echo $home; ?>"><img src="../images/img-assets/dashboard.png" class="icon-class"> Dashboard</a>
                    <a href="<?php echo BASE_URL."module/index.php?page=imunisasi" ?>" class="list-group-item list-group-item-action data <?php echo $imunisasi; ?>"><img src="../images/img-assets/anesthesia.png" class="icon-class"> Data Imunisasi</a>
                    <a href="<?php echo BASE_URL."module/index.php?page=petugas" ?>" class="list-group-item list-group-item-action data <?php echo $petugas; ?>"><img src="../images/img-assets/nurse.png" class="icon-class"> Data Petugas</a>
                    <a href="<?php echo BASE_URL."module/index.php?page=desa" ?>" class="list-group-item list-group-item-action data <?php echo $desa; ?>"><img src="../images/img-assets/maps-and-flags.png" class="icon-class"> Data Desa</a>
                    <a href="<?php echo BASE_URL."module/index.php?page=keluarga&sort=all" ?>" class="list-group-item list-group-item-action data <?php echo $keluarga ?>"><img src="../images/img-assets/family.png" class="icon-class"> Data Keluarga</a>
                </div>
            </div>

            <main class="col-md-9 ml-sm-auto col-lg-10 px-4" role="main">
                <?php 

                    $filename = "page/$page.php";
                
                    if (file_exists($filename)) {
                        include_once($filename);
                    }

                ?>

            </main>

        </div>

    </div>
</body>
    <script>

        $(document).ready(function() {
            $('#example').DataTable();
        });

    </script>

</html>