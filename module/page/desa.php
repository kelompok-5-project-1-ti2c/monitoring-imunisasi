<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h2>Data Desa</h2>
</div>

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-top pt-3 pb-2 mb-3 border-bottom">
 	<?php 

 		isset($_GET['form']) ? $_GET['form'] : false;
 		$form = $_GET['form'];

        if ($form == "desa_form") {

        	$filename = "page/form/desa_form.php";
            include_once($filename);

        }else if($form == "desa_detail"){

        	$desa_id = isset($_GET['desa_id']) ? $_GET['desa_id'] : false;

        	if($desa_id){

	            $filename = "page/form/desa_detail.php";
	            include_once($filename);

        	}else{

        		header("location:".BASE_URL."module/index.php?page=desa");

        	}
    	
    	}else{

    ?>

    	<div class="card mb-3 mr-3" style="width: 35rem; vertical-align: top;">
    		<div class="card-body">
    			<a href="<?php echo BASE_URL."module/index.php?page=desa&form=desa_form" ?>" class="btn btn-success">Tambah Data</a>
    		</div>
    		<div class="card-header bg-info text-white">
    			<h5>Tabel Desa</h5>
    		</div>
    		<div class="card-body">
    			
			    <table class="table table-bordered table-striped table-hover" id="example">
			  
				  <thead>
				    <tr>
				    	<th scope="col" width="20" class="text-center">No</th>
				     	<th scope="col">Nama Desa</th>
				      	<th scope="col" class="text-center" width="180">Aksi</th>
				    </tr>
				  </thead>
				  <tbody>
				  	
					<?php

						$query = mysqli_query($koneksi, "SELECT * FROM desa WHERE kode_puskesmas = '$kode_puskesmas' ORDER BY nama_desa ASC");
						$no = 1;
						while ($data = mysqli_fetch_array($query)) {
							
							echo "
								<tr>
									
									<td class='text-center'>$no</td>
									<td>$data[nama_desa]</td>
									<td class='text-center'>
										<a href='index.php?page=desa&form=desa_form&desa_id=$data[desa_id]' class='btn btn-outline-info'><i class='fa fa-edit'></i></a> 
										<a href='' class='btn btn-outline-danger'><i class='fa fa-trash-alt'></i></a>
										<a href='index.php?page=desa&form=desa_detail&desa_id=$data[desa_id]' class='btn btn-outline-success'><i class='fa fa-info-circle'></i></a>
									</td>
								</tr>
							";	
							$no++;
						}

					?>
				  
				  </tbody>
				</table>
    		</div>
    	</div>

    	<div class="card mb-3 mr-3" style="width: 51rem; vertical-align: top;">
    		<div class="card-body">
				<h5 class="card-title">Peta :</h5>
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d63439.18330524842!2d108.21626689645622!3d-6.400578864371666!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6eb8105ce03c53%3A0x8935fba66ad1211f!2sKec.%20Lohbener%2C%20Kabupaten%20Indramayu%2C%20Jawa%20Barat!5e0!3m2!1sid!2sid!4v1574062428448!5m2!1sid!2sid" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    		</div>
    	</div>
	<?php 

		} 	
	
	?>

</div>