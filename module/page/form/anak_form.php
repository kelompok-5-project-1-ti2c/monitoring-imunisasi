<?php
	$no_kk = isset($_GET['no_kk']) ? $_GET['no_kk'] : false;
	$anak_id = isset($_GET['anak_id']) ? $_GET['anak_id'] : false;
	$queryGet = mysqli_query($koneksi, "SELECT * FROM anak WHERE anak_id = '$anak_id'");
	$data = mysqli_fetch_array($queryGet);

	if($anak_id){

		$anak_id = $data['anak_id'];
		$nama_anak = $data['nama_anak'];
		$tempat_lahir = $data['tempat_lahir'];
		$tgl_lahir = $data['tgl_lahir'];

		$button = "Perbarui";

	}else{

		$no_kk = $_GET['no_kk'];
		$nama_anak = "";
		$tempat_lahir = "";
		$tgl_lahir = "";
		
		$button = "Simpan";
	}

?>
<div class="card card-success mb-3 mr-3" style="width: 60rem; vertical-align: top;">
    <div class="card-header bg-info text-white">
        <h5>Form Data Keluarga</h5>
    </div>
    <div class="card-body">

        <form method="post" action="<?php echo BASE_URL."module/page/aksi/anak_aksi.php?anak_id=$anak_id"; ?>">
		  
		  <div class="form-group">
		    <label><b>No. Kartu Keluarga</b></label>
		    <input type="text" name="no_kk" value="<?php echo $no_kk; ?>" class="form-control" readonly="readonly">
		  </div>

		  <div class="form-group">
		    <label><b>Nama Anak</b></label>
		    <input type="text" name="nama_anak" value="<?php echo $nama_anak; ?>" class="form-control" placeholder="Nama Anak">
		  </div>

		  <div class="form-group">
		    <label><b>Tempat Lahir</b></label>
		    <input type="text" name="tempat_lahir" value="<?php echo $tempat_lahir; ?>" class="form-control" placeholder="Cth: Indramayu">
		  </div>

		  <div class="form-group">
		    <label><b>Tanggal Lahir</b></label>
		    <input type="date" name="tgl_lahir" value="<?php echo $tgl_lahir; ?>" class="form-control">
		  </div>

		  <input type="submit" name="button" value="<?php echo $button; ?>" class="btn btn-info">
		  <input type="reset" value="Reset" class="btn btn-danger">

		</form>
    </div>
</div>

<div class="card mb-3 mr-3" style="width: 50rem; vertical-align: top;">
    <div class="card-body">
        <h5 class="card-title">Keterangan :</h5>
        <p class="card-text">
        	
        	Keterangan Form

        </p>
    </div>
</div>