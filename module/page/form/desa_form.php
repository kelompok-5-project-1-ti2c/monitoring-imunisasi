<?php
	$desa_id = isset($_GET['desa_id']) ? $_GET['desa_id'] : false;
	$queryGet = mysqli_query($koneksi, "SELECT * FROM desa WHERE desa_id = '$desa_id'");
	$data = mysqli_fetch_array($queryGet);

	if($desa_id){


		$desa_id = $data['desa_id'];
		$kode_puskesmas = $data['kode_puskesmas'];
		$nama_desa = $data['nama_desa'];
		$status = $data['status'];

		$button = "Perbarui";

	}else{

		$desa_id = "";
		$kode_puskesmas = $kode_puskesmas;
		$nama_desa = "";
		$status = "";

		$button = "Simpan";

	}

?>
<div class="card card-success mb-3 mr-3" style="width: 60rem; vertical-align: top;">
    <div class="card-header bg-info text-white">
        <h5>Form Data Desa</h5>
    </div>
    <div class="card-body">

        <form method="post" action="<?php echo BASE_URL."module/page/aksi/desa_aksi.php?desa_id=$desa_id"; ?>">
		  
		  <div class="form-group">
		    <input hidden type="text" name="desa_id" value="<?php echo $desa_id; ?>" class="form-control">
		    <input hidden type="text" name="kode_puskesmas" value="<?php echo $kode_puskesmas; ?>" class="form-control">
		  </div>

		  <div class="form-group">
		      <label><b>Nama Desa</b></label>
			    <input type="text" name="nama_desa" autocomplete="off" value="<?php echo $nama_desa; ?>" placeholder="Nama Desa" class="form-control">
          </div>

          <div class="form-group">
              <label><b>Status Desa</b></label><br>
                <?php
                    if($status == "on" || $status == ""){
                ?>
                    <input type="radio" name="status" value="on" checked> Aktif<br>
                    <input type="radio" name="status" value="off"> Tidak Aktif
                <?php
                     }else if($status == "off"){
                ?>
                        <input type="radio" name="status" value="on"> Aktif<br>
                        <input type="radio" name="status" value="off" checked> Tidak Aktif
                <?php
                     }
                ?>
           </div>

		  <input type="submit" name="button" value="<?php echo $button; ?>" class="btn btn-info">
		  <input type="reset" value="Reset" class="btn btn-danger">

		</form>
    </div>
</div>

<div class="card mb-3 mr-3" style="width: 50rem; vertical-align: top;">
    <div class="card-body">
        <h5 class="card-title">Keterangan :</h5>
        <p class="card-text">
        	
        	Keterangan Form

        </p>
    </div>
</div>