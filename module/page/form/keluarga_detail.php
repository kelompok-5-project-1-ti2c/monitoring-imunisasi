<?php
	
	$no_kk = $_GET['no_kk'];
	$queryOrangtua = mysqli_query($koneksi, "SELECT * FROM keluarga WHERE no_kk = '$no_kk'");
	$queryAnak = mysqli_query($koneksi, "SELECT * FROM anak WHERE no_kk = '$no_kk'");

	$data1 = mysqli_fetch_array($queryOrangtua);

	$nama_ayah 	= $data1['nama_ayah'];
	$nama_ibu 	= $data1['nama_ibu'];
	$no_hp 		= $data1['no_hp'];
	$alamat 	= $data1['alamat'];
	$desa 		= $data1['desa'];
	$kecamatan 	= $data1['kecamatan'];

?>
<div class="card mb-3 mr-3" style="width: 50rem; vertical-align: top;">
    <div class="card-header bg-info text-white">
        <h5>Data Orangtua</h5>
    </div>
    <div class="card-body">

       <table class="table table-bordered">
       		<thead>
       			<tr>
       				<th>No. Kartu Keluarga</th>
       				<td><?php echo $no_kk; ?></td>
       			</tr>
       			<tr>
       				<th>Nama Ayah</th>
       				<td><?php echo $nama_ayah; ?></td>
       			</tr>
       			<tr>
       				<th>Nama Ibu</th>
       				<td><?php echo $nama_ibu; ?></td>
       			</tr>
       			<tr>
       				<th>No. HP (Whatsapp)</th>
       				<td><?php echo $no_hp; ?></td>
       			</tr>
       			<tr>
       				<th>Alamat</th>
       				<td><?php echo $alamat; ?></td>
       			</tr>
       			<tr>
       				<th>Desa</th>
       				<th>Kecamatan</th>
       			</tr>
       		</thead>
       		<tbody>
       			<tr>
       				<td><?php echo $desa; ?></td>
       				<td><?php echo $kecamatan; ?></td>
       			</tr>
       		</tbody>
       </table>

    </div>
</div>

<div class="card mb-3 mr-3" style="width: 50rem; vertical-align: top;">
    <div class="card-header bg-info text-white">
        <h5>Data Anak</h5>
    </div>
    <div class="card-body">
        <p class="card-text">
        	
        	<table class="table table-bordered">
        		<thead>
        			<tr>
        				<th colspan="3">
        					<a href="<?php echo BASE_URL."module/index.php?page=keluarga&form=anak_form&no_kk=$data1[no_kk]" ?>" class="btn btn-success">Tambah data</a>
        				</th>
        			</tr>
        		</thead>
        		<thead>
        			<tr>
        				<th>Nama</th>
        				<th>Tempat, Tanggal Lahir</th>
        				<th class="text-center">Aksi</th>
        			</tr>
        		</thead>
        		<tbody>

					<?php

						if(mysqli_num_rows($queryAnak) == 0){
							echo "<tr>
									<td colspan=3>Belum ada data</td>
								  </tr>";
						}else{

							$no = 1;
							while ($data2 = mysqli_fetch_array($queryAnak)) {
								echo "
									<tr>
										<td>$data2[nama_anak]</td>
										<td>$data2[tempat_lahir], $data2[tgl_lahir]</td>
										<td class='text-center'>
											<a href='index.php?page=keluarga&form=anak_form&no_kk=$data1[no_kk]&anak_id=$data2[anak_id]' class='btn btn-info'><i class='fa fa-edit'></i></a>
										</td>
								";
							}

						}

					?>
        			
        		</tbody>
        	</table>

        </p>
    </div>
</div>