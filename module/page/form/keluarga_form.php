<?php
	$no_kk = isset($_GET['no_kk']) ? $_GET['no_kk'] : false;
	$queryGet = mysqli_query($koneksi, "SELECT * FROM keluarga WHERE no_kk = '$no_kk'");
	$queryDesa = mysqli_query($koneksi, "SELECT * FROM desa WHERE kode_puskesmas = '$kode_puskesmas'");
	$data = mysqli_fetch_array($queryGet);

	if($no_kk){


		$no_kk = $data['no_kk'];
		$nama_ayah = $data['nama_ayah'];
		$nama_ibu = $data['nama_ibu'];
		$no_hp = $data['no_hp'];
		$alamat = $data['alamat'];
		$desa = $data['desa'];
		$kecamatan = $data['kecamatan'];

		$button = "Perbarui";

	}else{

		$no_kk = "";
		$nama_ibu = "";
		$nama_ayah = "";
		$no_hp = "";
		$alamat = "";
		$desa = "";
		$kecamatan = "";

		$button = "Simpan";

	}

?>
<div class="card card-success mb-3 mr-3" style="width: 60rem; vertical-align: top;">
    <div class="card-header bg-info text-white">
        <h5>Form Data Keluarga</h5>
    </div>
    <div class="card-body">

        <form method="post" action="<?php echo BASE_URL."module/page/aksi/keluarga_aksi.php?no_kk=$no_kk"; ?>">
		  
		  <div class="form-group">
		    <label><b>No. Kartu Keluarga</b></label>
		    <input type="text" name="no_kk" value="<?php echo $no_kk; ?>" class="form-control" placeholder="Cth: 1234567890123123">
		  </div>

		  <div class="form-group">
		      <label><b>Nama Ayah dan Ibu</b></label>
			  <div class="input-group">	
			    <input type="text" name="nama_ayah" value="<?php echo $nama_ayah; ?>" placeholder="Nama Ayah" class="form-control">
			    <input type="text" name="nama_ibu" value="<?php echo $nama_ibu; ?>" placeholder="Nama Ibu" class="form-control">
			  </div>
		  </div>

		  <div class="form-group">
		    <label><b>Nomor HP (Whatsapp)</b></label>
		    <input type="text" name="no_hp" value="<?php echo $no_hp; ?>" class="form-control" placeholder="Cth: 08123456789">
		  </div>

		  <div class="form-group">
		    <label><b>Alamat</b></label>
		    <textarea class="form-control" name="alamat" placeholder="Alamat lengkap"><?php echo $alamat; ?></textarea>
		  </div>

		  <div class="form-group">
		      <label><b>Desa | Kecamatan</b></label>
			  <div class="input-group">	
			    <select class="form-control" name="desa">
					<?php
						if($no_kk){
							
							echo "<option value='$desa' checked>$desa</option>";
							while ($row = mysqli_fetch_assoc($queryDesa)) {
							
									echo "<option value='$row[nama_desa]'>$row[nama_desa]</option>";						
							
							}
							
						}else{
							
							echo "<option>----Pilih----</option>";
							while ($row = mysqli_fetch_assoc($queryDesa)) {
							
									echo "<option value='$row[nama_desa]'>$row[nama_desa]</option>";						
							
							}

						}
						
						
			    	?>

			    </select>
			    <input type="text" name="kecamatan" value="<?php echo $_SESSION['kecamatan']; ?>" placeholder="Kecamatan" class="form-control" readonly="readonly">
			  </div>
		  </div>

		  <input type="submit" name="button" value="<?php echo $button; ?>" class="btn btn-info">
		  <input type="reset" value="Reset" class="btn btn-danger">

		</form>
    </div>
</div>

<div class="card mb-3 mr-3" style="width: 50rem; vertical-align: top;">
    <div class="card-body">
        <h5 class="card-title">Keterangan :</h5>
        <p class="card-text">
        	
        	Keterangan Form

        </p>
    </div>
</div>