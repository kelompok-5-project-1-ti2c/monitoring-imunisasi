<?php
	$petugas_id = isset($_GET['petugas_id']) ? $_GET['petugas_id'] : false;
	$queryGet = mysqli_query($koneksi, "SELECT * FROM petugas WHERE petugas_id = '$petugas_id'");
	$queryDesa = mysqli_query($koneksi, "SELECT * FROM desa WHERE kode_puskesmas = '$kode_puskesmas' ORDER BY nama_desa ASC");
	$data = mysqli_fetch_array($queryGet);

	if($petugas_id){


		$petugas_id = $data['petugas_id'];
		$kode_puskesmas = $data['kode_puskesmas'];
		$desa_id = $data['desa_id'];
		$nama_petugas = $data['nama_petugas'];
		$status = $data['status'];

		$button = "Perbarui";

	}else{

		$petugas_id = "";
		$desa_id = "";
		$kode_puskesmas = $kode_puskesmas;
		$nama_petugas = "";
		$status = "";

		$button = "Simpan";

	}

?>
<div class="card card-success mb-3 mr-3" style="width: 60rem; vertical-align: top;">
    <div class="card-header bg-info text-white">
        <h5>Form Data Petugas</h5>
    </div>
    <div class="card-body">

        <form method="post" action="<?php echo BASE_URL."module/page/aksi/petugas_aksi.php?petugas_id=$petugas_id"; ?>">
		  
		  <div class="form-group">
		    <input hidden type="text" name="petugas_id" value="<?php echo $petugas_id; ?>" class="form-control">
		    <input hidden type="text" name="kode_puskesmas" value="<?php echo $kode_puskesmas; ?>" class="form-control">
		  </div>

		  <div class="form-group">
		    <label><b>Nama Petugas</b></label>
			<input type="text" name="nama_petugas" value="<?php echo $nama_petugas; ?>" placeholder="Nama Petugas" class="form-control">
			  
          </div>
          
		  <div class="form-group">
		      <label><b>Desa | Kecamatan</b></label>
			  <div class="input-group">	
			    <select class="form-control" name="desa_id">
					<?php
						if($petugas_id){
							
							echo "<option value='$desa_id' checked>$desa_id</option>";
							while ($row = mysqli_fetch_assoc($queryDesa)) {
							
									echo "<option value='$row[desa_id]'>$row[nama_desa]</option>";						
							
							}
							
						}else{
							
							echo "<option>----Pilih----</option>";
							while ($row = mysqli_fetch_assoc($queryDesa)) {
							
									echo "<option value='$row[desa_id]'>$row[nama_desa]</option>";						
							
							}

						}
						
						
			    	?>

			    </select>
			    <input type="text" value="<?php echo $_SESSION['kecamatan']; ?>" placeholder="Kecamatan" class="form-control" readonly="readonly">
			  </div>
          </div>
          
          <div class="form-group">
              <label><b>Status Petugas</b></label><br>
                <?php
                    if($status == "on" || $status == ""){
                ?>
                    <input type="radio" name="status" value="on" checked> Aktif<br>
                    <input type="radio" name="status" value="off"> Tidak Aktif
                <?php
                     }else if($status == "off"){
                ?>
                        <input type="radio" name="status" value="on"> Aktif<br>
                        <input type="radio" name="status" value="off" checked> Tidak Aktif
                <?php
                     }
                ?>
           </div>

		  <input type="submit" name="button" value="<?php echo $button; ?>" class="btn btn-info">
		  <input type="reset" value="Reset" class="btn btn-danger">

		</form>
    </div>
</div>

<div class="card mb-3 mr-3" style="width: 50rem; vertical-align: top;">
    <div class="card-body">
        <h5 class="card-title">Keterangan :</h5>
        <p class="card-text">
        	
        	Keterangan Form

        </p>
    </div>
</div>