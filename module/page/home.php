<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h2>Dashboard</h2>
</div>

<div class="d-flex flex-wrap flex-md-nowrap pt-3 pb-2 mb-3 border-bottom">
    <div class="card text-white card1 mb-3 mr-3" style="width: 20rem; height: 10rem;">
        <div class="card-body">
            <h5 class="card-title">Data Desa</h5>
            <p class="card-text"></p>
        </div>
    </div>
    <div class="card text-white card2 mb-3 mr-3" style="width: 20rem; height: 10rem;">
        <div class="card-body">
            <h5 class="card-title">Data Imunisasi</h5>
            <p class="card-text"></p>
        </div>
    </div>
    <div class="card text-white card3 mb-3 mr-3" style="width: 20rem; height: 10rem;">
        <div class="card-body">
            <h5 class="card-title">Data Keluarga</h5>
            <p class="card-text"></p>
        </div>
    </div>
</div>