<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h2>Data Imunisasi</h2>
</div>

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-top pt-3 pb-2 mb-3 border-bottom">
 	<?php 

 		isset($_GET['form']) ? $_GET['form'] : false;
 		$form = $_GET['form'];

        if ($form == "imunisasi_form") {

        	$filename = "page/form/imunisasi_form.php";
            include_once($filename);

        }else if($form == "imunisasi_detail"){

        	$petugas_id = isset($_GET['kode_imunisasi']) ? $_GET['kode_imunisasi'] : false;

        	if($kode_imunisasi){

	            $filename = "page/form/imunisasi_detail.php";
	            include_once($filename);

        	}else{

        		header("location:".BASE_URL."module/index.php?page=imunisasi");

        	}
    	
    	}else{

    ?>

    	<div class="card mb-3 mr-3" style="width: 65rem; vertical-align: top;">
    		<div class="card-header bg-info text-white">
    			<h5>Tabel Imunisasi</h5>
    		</div>
    		<div class="card-body">
    			
			    <table class="table table-bordered table-striped table-hover" id="example">
			  
				  <thead>
				    <tr>
				    	<th scope="col" width="20" class="text-center">No</th>
				     	<th scope="col">Nama Vaksin Imunisasi</th>
				     	<th scope="col" class="text-center">Bulan Pemberian</th>
				    </tr>
				  </thead>
				  <tbody>
				  	
					<?php

						$query = mysqli_query($koneksi, "SELECT * FROM imunisasi ORDER BY bulan ASC");
						$no = 1;
						while ($data = mysqli_fetch_array($query)) {
							
							echo "
								<tr>
									
									<td class='text-center'>$no</td>
									<td>$data[nama_imunisasi]</td>
									<td class='text-center'>Bulan ke - $data[bulan]</td>
								</tr>
							";	
							$no++;
						}

					?>
				  
				  </tbody>
				</table>
    		</div>
    	</div>

    	<div class="card mb-3 mr-3" style="width: 30rem; vertical-align: top;">
    		<div class="card-body">
    			<h5 class="card-title">Riwayat :</h5>
    		</div>
    	</div>
	<?php 

		} 	
	
	?>

</div>