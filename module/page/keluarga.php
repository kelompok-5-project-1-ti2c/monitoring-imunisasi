<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
	<h2>Data Keluarga</h2>
</div>

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-top pt-3 pb-2 mb-3 border-bottom">
 	<?php 

 		isset($_GET['form']) ? $_GET['form'] : false;
		$form = $_GET['form'];

        if ($form == "keluarga_form") {

        	$filename = "page/form/keluarga_form.php";
			include_once($filename);

        }else if($form == "keluarga_detail"){

        	$no_kk = isset($_GET['no_kk']) ? $_GET['no_kk'] : false;

        	if($no_kk){

	            $filename = "page/form/keluarga_detail.php";
				include_once($filename);

        	}else{

        		header("location:".BASE_URL."module/index.php?page=keluarga");

        	}
    	
    	}else if($form == "anak_form"){

	            $filename = "page/form/anak_form.php";
				include_once($filename);
    	
    	}else{

    ?>

    	<div class="mb-3 mr-3" style="width: 65rem; vertical-align: top;">
    		<div class="card-body">
				<a href="<?php echo BASE_URL."module/index.php?page=keluarga&form=keluarga_form" ?>" class="btn btn-success">Daftar Baru</a>
				<form action=""  method="get" class="d-inline float-right mr-auto pr-0">
					<div class="input-group">
						<div class="input-group-prepend">
							<label class="input-group-text">Sort</label>
						</div>
						<select name="desa" class="form-control col-12 d-inline">
							
							<?php

								$sortDesa = mysqli_query($koneksi, "SELECT * FROM desa WHERE kode_puskesmas = '$kode_puskesmas'");
								if(mysqli_num_rows($sortDesa)== 0){
									echo "<option>Belum ada data Desa</option>";
								}else{
																		
									echo "<option value='all'>---Show All---</option>";
									while ($sortData = mysqli_fetch_assoc($sortDesa)) {
										
										echo "<option value='$sortData[nama_desa]'>$sortData[nama_desa]</option>";
										
									}
								
								}
							
							?>

						</select>
						<div class="input-group-prepend">
							<button class="btn btn-outline-info input-group">Cari</button>
						</div>
					</div>
				</form>
			</div>
			
    		<div class="card-header bg-info text-white">
    			<h5>Tabel Keluarga</h5>
    		</div>
    		<div class="card-body">
    			
			    <table class="table table-bordered table-striped table-hover" id="example">
			  
				  <thead>
				    <tr class="text-center">
				    	<!-- <th scope="col" rowspan="2" width="20" class="align-middle">No</th> -->
				     	<th scope="col" rowspan="2" width="50" class="align-middle">No. Kartu Keluarga</th>
				     	<th scope="col" colspan="2">Nama</th>
				      	<th scope="col" class="align-middle" rowspan="2" width="180">Aksi</th>
				    </tr>
				    <tr class="text-center">
				    	<th scope="col" width="150">Ayah</th>
						<th scope="col" width="150">Ibu</th>
				    </tr>
				  </thead>
				  <tbody>
				  	
					<?php

						if($sort != false){

							$query = mysqli_query($koneksi, "SELECT * FROM keluarga WHERE kecamatan = '$kecamatan' AND desa LIKE '$sort%'");
							$queryAnak = mysqli_query($koneksi, "SELECT * FROM keluarga JOIN anak ON keluarga.no_kk = anak.no_kk WHERE desa ='$sort'");
														
							
						}
						if($sort == 'all' || $sort == ""){
							
							$query = mysqli_query($koneksi, "SELECT * FROM keluarga WHERE kecamatan = '$kecamatan'");
							$queryAnak = mysqli_query($koneksi, "SELECT * FROM keluarga JOIN anak ON keluarga.no_kk = anak.no_kk");
						
						}

						$countKeluarga = mysqli_num_rows($query);
						$countAnak = mysqli_num_rows($queryAnak);
									
						$no = 1;
						while ($data = mysqli_fetch_array($query)) {
							
							echo "
								<tr class='text-center'>
								
									<td>$data[no_kk]</td>
									<td>$data[nama_ayah]</td>
									<td>$data[nama_ibu]</td>
									<td>
										<a href='index.php?page=keluarga&form=keluarga_form&no_kk=$data[no_kk]' class='btn btn-outline-info'><i class='fa fa-edit'></i></a> 
										<a href='index.php?page=keluarga&sort=$sort&alert=hapus' class='btn btn-outline-danger'><i class='fa fa-trash-alt'></i></a>
										<a href='index.php?page=keluarga&form=keluarga_detail&no_kk=$data[no_kk]' class='btn btn-outline-success'><i class='fa fa-info-circle'></i></a>
									</td>

								</tr>
							";	
							$no++;
						}
						
						
					?>
				  
				  </tbody>
				</table>
    		</div>
    	</div>

    	<div style="width: 40rem;">
		
			<div class="card">

				<div class="card-body">
					<h5 class="card-title">Sort : 
						<?php

							if($sort != "all" && $sort == $sort){
								echo "<span class='bg-info text-white p-2'>Desa ".$sort."</span>";
							}
							if($sort == "all"){
								echo "<span class='bg-info text-white p-2'>All Data</span>";
							}
							
						?>
					</h5><br>
					<table class="table table-bordered">
						<tr>
							<td width=150 class="text-center">Jumlah Keluarga</td>
							<td class="bg-warning text-white pb-0 pt-2">
								<h3><b><?php echo $countKeluarga; ?> </b></h3>
							</td>
						</tr>
					</table>
					<table class="table table-bordered">
						<tr>
							<td width=150 class="text-center">Jumlah Anak</td>
							<td class="bg-danger text-white pb-0 pt-2">
								<h3><b><?php echo $countAnak; ?> </b></h3>
							</td>
						</tr>
					</table>
					
				</div>
			
			</div>
			<div class="card">

				<div class="card-body">
					<h5 class="card-title">Riwayat (Log) :</h5>
					
					<?php

						$queryLog = mysqli_query($koneksi, "SELECT * FROM keluarga_log WHERE kecamatan = '$nama_puskesmas' ORDER BY keluarga_log_id DESC LIMIT 6");
						while ($log = mysqli_fetch_assoc($queryLog)) {

							if($log['kejadian'] == "Menambahkan data"){
								$alertmode = "alert-success";
							}else if ($log['kejadian'] == "Menghapus data") {
								$alertmode = "alert-danger";
							}else if ($log['kejadian'] == "Mengubah data") {
								$alertmode = "alert-warning";
							}

							echo "
								<div class='alert $alertmode' style='font-size: 13px;' role='alert'>
									$log[kejadian] Keluarga Bpk <b><a href='index.php?page=keluarga&form=keluarga_detail&no_kk=$log[no_kk]'>$log[nama_ayah]</a></b> | <span class='bg-white p-1'>$log[waktu]</span>
								</div>
							";
						
						}

					?>
				</div>
					
			</div>
		
		</div>
		<?php 

		} 	
	
	?>

</div>