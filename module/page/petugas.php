<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h2>Data Petugas Imunisasi</h2>
</div>

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-top pt-3 pb-2 mb-3 border-bottom">
 	<?php 

 		isset($_GET['form']) ? $_GET['form'] : false;
 		$form = $_GET['form'];

        if ($form == "petugas_form") {

        	$filename = "page/form/petugas_form.php";
            include_once($filename);

        }else if($form == "petugas_detail"){

        	$petugas_id = isset($_GET['petugas_id']) ? $_GET['petugas_id'] : false;

        	if($petugas_id){

	            $filename = "page/form/petugas_detail.php";
	            include_once($filename);

        	}else{

        		header("location:".BASE_URL."module/index.php?page=petugas");

        	}
    	
    	}else{

    ?>

    	<div class="card mb-3 mr-3" style="width: 65rem; vertical-align: top;">
    		<div class="card-body">
    			<a href="<?php echo BASE_URL."module/index.php?page=petugas&form=petugas_form" ?>" class="btn btn-success">Tambah Data</a>
    		</div>
    		<div class="card-header bg-info text-white">
    			<h5>Tabel Petugas</h5>
    		</div>
    		<div class="card-body">
    			
			    <table class="table table-bordered table-striped table-hover" id="example">
			  
				  <thead>
				    <tr>
				    	<th scope="col" width="20" class="text-center">No</th>
				     	<th scope="col">Nama petugas</th>
				     	<th scope="col" class="text-center">Status</th>
				      	<th scope="col" class="text-center" width="180">Aksi</th>
				    </tr>
				  </thead>
				  <tbody>
				  	
					<?php

						$query = mysqli_query($koneksi, "SELECT * FROM petugas WHERE kode_puskesmas = '$kode_puskesmas' ORDER BY nama_petugas ASC");
						$no = 1;
						while ($data = mysqli_fetch_array($query)) {
							
							echo "
								<tr>
									
									<td class='text-center'>$no</td>
									<td>$data[nama_petugas]</td>
									<td class='text-center'>$data[status]</td>
									<td class='text-center'>
										<a href='index.php?page=petugas&form=petugas_form&petugas_id=$data[petugas_id]' class='btn btn-outline-info'><i class='fa fa-edit'></i></a> 
										<a href='' class='btn btn-outline-danger'><i class='fa fa-trash-alt'></i></a>
										<a href='index.php?page=petugas&form=petugas_detail&petugas_id=$data[petugas_id]' class='btn btn-outline-success'><i class='fa fa-info-circle'></i></a>
									</td>
								</tr>
							";	
							$no++;
						}

					?>
				  
				  </tbody>
				</table>
    		</div>
    	</div>

    	<div class="card mb-3 mr-3" style="width: 30rem; vertical-align: top;">
    		<div class="card-body">
    			<h5 class="card-title">Riwayat :</h5>
    		</div>
    	</div>
	<?php 

		} 	
	
	?>

</div>